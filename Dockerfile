FROM ubuntu

MAINTAINER Lars Eppinger <lars.eppinger@posteo.de>

RUN apt-get update --fix-missing -y
RUN apt-get install -y openjdk-8-jre golang-go wget unzip

RUN cd /root && \
    wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-linux.zip && \
    unzip -d sonar-scanner sonar-scanner-cli-3.2.0.1227-linux.zip && \
    mv sonar-scanner/* sonar_home

RUN mkdir -p /go/src $$ mkdir -p /go/bin

ENV GOPATH=/go
ENV GOBIN=${GOPATH}/bin
ENV SONAR_RUNNER_HOME=/root/sonar_home
ENV PATH ${SONAR_RUNNER_HOME}/bin:$PATH

CMD sonar-scanner -Dsonar.projectBaseDir=/root/src
