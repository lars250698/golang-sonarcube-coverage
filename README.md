# Golang Sonarqube Coverage

A docker image to run Golang builds with test coverage for Sonarqube.

## Usage

Example .gitlab-ci.yaml

``` yaml
image: docker:stable

variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
    IMAGE: registry.gitlab.com/user/myproject

stages:
    - analysis

analysis:
    stage: analysis
    image: leppinger/golang-sonarqube-coverage:latest
    script:
        - mv /builds $GOPATH/src/gitlab.com
        - cd $GOPATH/src/gitlab.com/user/myproject
        - go test -coverprofile=coverage.out ./...
        - sonar-scanner -Dsonar.projectKey=my-project -Dsonar.sources=. -Dsonar.host.url=https://sonar.mydomain.com -Dsonar.login=$SONAR_TOKEN
    tags:
        - docker
```